package com.zuitt.example;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;

public class s2A2 {
    public static void main(String[] args) {
    // ARRAY
     // Create  method and declare an array for containing the first 5 prime numbers
         int[] intArray = new int[5];

     //Assign the first 5 prime numbers to their respective indices
         intArray[0] = 2;
        intArray[1] = 3;
        intArray[2] = 5;
        intArray[3] = 7;
        intArray[4] =11;

        // int [] primeNumber = {2, 3, 5, 7, 11};

     // Output specific elements of the array in the terminal by specifying the index of the target element

        System.out.println("The first prime number is: " + intArray[0]);
        System.out.println("The second prime number is: " + intArray[1]);
        System.out.println("The third prime number is: " + intArray[2]);
        System.out.println("The fourth prime number is: " + intArray[3]);
        System.out.println("The fifth prime number is: " + intArray[4]);


    // ARRAYLIST
        // Create an ArrayList of String data-type elements using generics and add 4 elements to it.
        String[] names = {"John", "Jane", "Chloe", "Zoey"};
        System.out.println("My friends are: " + Arrays.toString(names));


    //  HASHMAP
        // Using generics, create a HashMap of keys with data type String and values with data type Integer
        HashMap<String, Integer> productStock = new HashMap<String, Integer>();

            productStock.put("toothpaste", 15);
            productStock.put("toothbrush", 20);
            productStock.put("soap", 12);

        //Output the contents of the HasMap concatenated with a string message in the console
            System.out.println("Our current inventory is: " + productStock);

    }
}
